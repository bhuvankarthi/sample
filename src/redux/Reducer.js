const Reducer = (state = { value: "", blog: [] }, action) => {
  switch (action.type) {
    case "CHANGE_EVENT":
      return {
        ...state,
        value: action.payload,
      };

    case "ADD_BLOG":
      return {
        ...state,
        blog: [state.value, ...state.blog],
        value: "",
      };

    default:
      return state;
  }
};
export default Reducer;
