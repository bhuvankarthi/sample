import Reducer from "./Reducer";
import { combineReducers } from "redux";

export const CombinedReducers = combineReducers({
    Reducer
})