import React from "react";
import { useSelector } from "react-redux";
import "./Blog.css";
function Blog(props) {
  const { value } = props;

  return <div className="separate-blog">{value}</div>;
}

export default Blog;
