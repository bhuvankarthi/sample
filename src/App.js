import { useSelector, useDispatch } from "react-redux";
import "./App.css";
import Blog from "./components/Blog";
import { v4 as uuid } from "uuid";

function App() {
  let dispatch = useDispatch();
  let state = useSelector((state) => {
    return state.Reducer;
  });
  const inputChangeHandler = (event) => {
    let data = event.target.value;

    dispatch({ type: "CHANGE_EVENT", payload: data });
  };

  const buttonHandler = () => {
    dispatch({ type: "ADD_BLOG" });
  };

  return (
    <>
      <div className="App">
        <input
          type="textbox"
          onChange={inputChangeHandler}
          value={state.value}
        />
        <button onClick={buttonHandler}>Add</button>
      </div>
      <div className="blog-div">
        {state.blog.map((blog) => {
          let uuidKey = uuid();
          return <Blog value={blog} key={uuidKey} />;
        })}
      </div>
    </>
  );
}

export default App;
